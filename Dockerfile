FROM ubuntu:bionic
LABEL Description="New python container"

#installation python avec les différents packages et librairies sous ubuntu  
RUN apt-get update
RUN apt-get install -y tzdata
RUN apt-get install -y software-properties-common

RUN apt-get install -y wget git python3-dev  python3.7 libpython3.7-dev curl python3-pip
RUN apt-get install -y rsyslog && rsyslogd

RUN python3.7 -m pip install tensorflow pandas matplotlib sympy numpy networkx pillow
#préciser la memoire du travail et le code qui sera s'exécuter

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["python -m unittest","code.py"]





